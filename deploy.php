<?php
namespace Deployer;

require 'recipe/laravel.php';
require 'recipe/npm.php';

set('application', 'soho');
set('repository', 'git@bitbucket.org:sigmeo/soho.git');
set('git_tty', false);
set('ssh_type', 'native');
set('ssh_multiplexing', false);
set('default_timeout', null);

add('shared_files', []);
add('shared_dirs', []);

add('writable_dirs', []);
set('allow_anonymous_stats', false);

// set('bin/composer', function () {
//     run("cd {{release_path}} && curl -sS https://getcomposer.org/installer | {{bin/php}}");
//     $composer = '{{release_path}}/composer.phar';

//     return '{{bin/php}} ' . $composer;
// });

set('bin/composer', '{{bin/php}} ~/composer/composer');

host('85.194.246.64')
    ->stage('dev')
    ->user('soho')
    ->set('branch', 'master')
    ->set('deploy_path', '/home/{{application}}/domains/{{application}}.sigdev.pl/code/')
    ->set('bin/php', function(){
        return '/usr/local/php73/bin/php';
    });

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'npm:install',
    'npm:build',
    'artisan:storage:link',
//    'artisan:view:cache', // TODO: zakomentowałem, bo psuje się livewire przy imporcie mt940 :(  do rozwiązania
    'artisan:config:cache',
    // 'artisan:optimize',
    'artisan:horizon:terminate',
    'artisan:route:clear',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);

task('artisan:route:clear', function () {
    run('{{bin/php}} {{release_path}}/artisan route:clear');
});

task('npm:build', function () {
    run('cd {{release_path}} && {{bin/npm}} run production');
});

task('build', function () {
    run('cd {{release_path}} && build');
});

after('deploy:failed', 'deploy:unlock');

before('deploy:symlink', 'artisan:migrate');

